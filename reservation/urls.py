from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
# from .admin import admin_desktop, admin_mobile
from django.conf import settings

admin.site.site_url = None

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^scheduler/', include('apps.schedule.urls')),
    # url(r'^admin-desktop/', include(admin_desktop.urls)),
    # url(r'^admin-mobile/', include(admin_mobile.urls)),
    url(r'^', include(admin.site.urls)),
]


if settings.DEBUG:
    urlpatterns += patterns('',
                           url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                               {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
                           ) + staticfiles_urlpatterns() + urlpatterns
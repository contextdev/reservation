from django.conf import settings # import the settings file

def add_site_id(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'SITE_ID': settings.SITE_ID }
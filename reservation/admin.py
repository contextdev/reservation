from django.contrib import admin

from django.contrib.sites.models import Site
from django.contrib.auth.models import Group

from django.conf import settings

if settings.SITE_ID != 1:
    # admin.site.unregister([Site, Group])
    admin.site.unregister([Site])


#
# from django.contrib import admin
#
# from apps.schedule.models import Rule, Event, Calendar
#
# from django.contrib.auth.models import User, Group
#
# class AdminDesktop(AdminSite):
#     site_header = 'Admin desktop'
#     site_title = "Admin desktop"
#     index_template = None
#     app_index_template = None
#
#     site_url = '/admin-desktop/'
#
#
#
# admin_desktop = AdminDesktop(name='admin_desktop') #AdminDesktop(name='admin_desktop')
#
#
# admin_desktop.register([Rule, User, Group])
#
#
# class AdminMobile(AdminSite):
#     site_header = 'Admin mobile'
#
# admin_mobile = AdminMobile(name='admin_mobile')
# admin_mobile.register([Event, User, Group])

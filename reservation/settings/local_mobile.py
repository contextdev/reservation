from .base import *


DEBUG = True

TEMPLATE_DEBUG = DEBUG

INSTALLED_APPS += (
    'django_extensions',   # for Ipython Notebook
    'debug_toolbar',

)
SITE_ID = 3

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

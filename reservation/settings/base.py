
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'o+_q^gxv+d$iu!$r5m0$743sc#s@6t&sz3qt9@#ogb7&s%ogwx'

DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'jet.dashboard',
    'jet',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'django_cleanup',

    "geoposition",
    #'mptt',
    #'django_mptt_admin',
    #'import_export',
    'wkhtmltopdf',
    'djmail',
    'reservation',
    'apps.generic',
    'apps.schedule',
    'apps.person',
    'apps.treatment',
    'apps.clinic_history',
    'apps.payment',
    'apps.notification'

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'reservation.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'reservation.context_processors.add_site_id',
                "django.core.context_processors.tz"
            ],
        },
    },
]

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

WSGI_APPLICATION = 'reservation.wsgi.application'


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATIC_ROOT = os.path.join(os.path.dirname(os.path.dirname(BASE_DIR)), "static")



STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"), )

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = "/media/"


###### EMAIL CONFIG

DEFAULT_FROM_EMAIL = 'testnubiquo@gmail.com'

EMAIL_BACKEND="djmail.backends.async.EmailBackend"
DJMAIL_REAL_BACKEND="django.core.mail.backends.smtp.EmailBackend"

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = DEFAULT_FROM_EMAIL
EMAIL_HOST_PASSWORD = 'nubiquo1234567890'
EMAIL_USE_TLS = True


############### JET - ADMIN THEME CONFIG
JET_THEMES = [
    {
        'theme': 'default', # theme folder name
        'color': '#47bac1', # color of the theme's button in user menu
        'title': 'Default' # theme title
    },
    {
        'theme': 'green',
        'color': '#44b78b',
        'title': 'Green'
    },
    {
        'theme': 'light-green',
        'color': '#2faa60',
        'title': 'Light Green'
    },
    {
        'theme': 'light-violet',
        'color': '#a464c4',
        'title': 'Light Violet'
    },
    {
        'theme': 'light-blue',
        'color': '#5EADDE',
        'title': 'Light Blue'
    },
    {
        'theme': 'light-gray',
        'color': '#222',
        'title': 'Light Gray'
    }
]

JET_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboardDesktop'


############# SCHEDULER APP CONFIG
SHOW_CANCELLED_OCCURRENCES = False

OCCURRENCE_CANCEL_REDIRECT = '/scheduler/fullcalendar/treatments/'


LOGIN_URL = '/login/'


TIME_INPUT_FORMATS = ('%I:%M %p',)


#SESSION_EXPIRE_AT_BROWSER_CLOSE = True
#SESSION_COOKIE_AGE = 60 * 500 #
#SESSION_ENGINE = 'django.contrib.sessions.backends.cache'


from celery.schedules import crontab
from datetime import timedelta

BACKUP_FOLDER = os.path.join(BASE_DIR, 'backups')

CELERYBEAT_SCHEDULE = {
    'alert': {
        'task': 'reservation.tasks.alert',
        'schedule': timedelta(seconds=10),
        # 'schedule': crontab(minute=0, hour=0),
    },
    'occurrence_cancel': {
        'task': 'reservation.tasks.cancel_old_occurrences',
        'schedule': timedelta(seconds=10),
        # 'schedule': crontab(minute=0, hour=0),
    }
}

CELERY_TIMEZONE = 'UTC'



from .base import *

DEBUG = False

TEMPLATE_DEBUG = DEBUG

SITE_ID = 2

ALLOWED_HOSTS = ["*"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
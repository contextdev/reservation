from __future__ import absolute_import


from celery import shared_task
from django.db.models import Q
from apps.schedule.models import Calendar, Occurrence
from apps.notification.models import Notification
from datetime import date, datetime, timedelta
from apps.schedule.periods import Period
import pytz

from .celery import app


import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'reservation.settings')

# @app.task
# @shared_task
# def backup():
#     print(30*"===")
#     print(10*"BACKUP")
#     #cmd = "pg_dump -U " + settings.DATABASES['default']['USER'] + " " + settings.DATABASES['default']['NAME'] + " -f " + settings.BACKUP_FOLDER  + "/bkp-" + datetime.datetime.now().strftime("%y-%m-%d-%H-%M") + ".sql"
#     #print(cmd)
#     #r = subprocess.call(cmd, shell=True)
#
#
# @app.task
# @shared_task
# def check_occurrences():
#     start = datetime.today()
#     end = datetime.today() + timedelta(hours=1)
#     calendar = Calendar.objects.first()
#     event_list = calendar.events.filter(start__lte=end).filter(
#             Q(end_recurring_period__gte=start) | Q(end_recurring_period__isnull=True) )
#
#     for event in event_list:
#         occurrences = event.get_occurrences(start, end)
#         for occurrence in occurrences:
#             print(occurrence)


@app.task
@shared_task
def alert():
    now = datetime.today()

    start = datetime(now.year, now.month, now.day, 0, 0, tzinfo=pytz.utc)
    # Se le suma una hora para que pueda encontar occurrencias futuras a 1 hora como maximo, para poder avisar
    end = datetime(now.year, now.month, now.day, now.hour + 1, now.minute, tzinfo=pytz.utc)

    now_utc = datetime(now.year, now.month, now.day, now.hour, now.minute, tzinfo=pytz.utc)
    calendar = Calendar.objects.first()
    event_list = calendar.events.filter(start__lte=end).filter(
            Q(end_recurring_period__gte=start) | Q(end_recurring_period__isnull=True) )
    for event in event_list:
        if event and event.alert != 0:
            occurrences = event.get_occurrences(start, end)
            for occurrence in occurrences:
                has_pk = True # If not has pk is new occurrence => hasnt notification
                if not occurrence.id:
                    has_pk = False
                    occurrence.save()

                if not has_pk or not len(Notification.objects.for_object(occurrence, Occurrence)):
                    future_occurrences_without_delta = occurrence.start - timedelta(minutes=event.alert)
                    if future_occurrences_without_delta <= now_utc:
                        # Create notification to therapist
                        message = """ This message is to remind you that at %s you have an appointment with
                        """ %(occurrence.start)

                        # Create notification to therapist
                        Notification(obj=occurrence,
                                     user_obj=occurrence.therapist.user,
                                     message=message + "paciente %s" % occurrence.patient.__str__()).save()
                        # Create notification to patient
                        Notification(obj=occurrence,
                                     user_obj=occurrence.patient,
                                     message=message + "terapeuta %s" % occurrence.therapist.__str__()).save()




@app.task
@shared_task
def cancel_old_occurrences():
    """

    ESTO NO ESTA PROBADO

    """

    now = datetime.today()
    yesterday = datetime.now() - timedelta(days=1)
    two_daysy_back = datetime.now() - timedelta(days=2)

    calendar = Calendar.objects.first()
    if calendar:
        event_list = calendar.events.filter(start__lte=yesterday)

        yesterday_period = Period(event_list, two_daysy_back, yesterday)

        occurrences = yesterday_period.get_occurrences()

        for occurrence in occurrences:
            if occurrence.status == 1:
                occurrence.cancel()


from __future__ import absolute_import, unicode_literals

import os
import warnings


BASE_DIR =  os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

# Environment
ENVIRONMENT = os.environ.get('ENVIRONMENT', 'dev')
if ENVIRONMENT == 'dev_local':
    DEVNAME = os.environ['DEVNAME']

DEBUG = ENVIRONMENT in ('dev_local', 'dev', 'test')
TEMPLATE_DEBUG = DEBUG


if not os.environ.get('SECRET_KEY'):
    warnings.warn((
                      "Please define SECRET_KEY before importing {0}, as a fallback "
                      "for when the environment variable is not available."
                  ).format(__name__))
else:
    SECRET_KEY = os.environ.get('SECRET_KEY')


################## APPS CONFIG #############################

INSTALLED_APPS = (
    'jet.dashboard',
    'jet',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'django_cleanup',

    "geoposition",
    #'mptt',
    #'django_mptt_admin',
    #'import_export',
    'wkhtmltopdf',
    'djmail',
    'reservation',
    'apps.generic',
    'apps.schedule',
    'apps.person',
    'apps.treatment',
    'apps.clinic_history',
    'apps.payment',
    'apps.notification'

)

############## DATABASE CONFIG #############

DATABASES = {
    'default': {
        'ENGINE': os.environ['DB_ENGINE'],
        'NAME': os.environ['DB_NAME'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASS'],
        'HOST': os.environ['DB_HOST'],
        'PORT': os.environ['DB_PORT'],
    }
}



MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'reservation.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(SITE_ROOT, "templates"),
            ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.core.context_processors.static',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'reservation.context_processors.add_site_id',
                "django.core.context_processors.tz"
            ],
            },
        },
    ]

WSGI_APPLICATION = 'reservation.wsgi.application'




# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'

# Defined folder of static files to projects
STATICFILES_DIRS = (
    os.path.join(SITE_ROOT, "static"),
)

STATIC_ROOT = os.path.join(BASE_DIR, "static_root")

# Defined folder of media files to project
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"


######### AUTHENTICATION CONFIG ################


LOGIN_URL = '/login/'


########## LOGGIN CONFIG ####################


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(filename)s][%(funcName)s][%(lineno)d][%(name)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        },
    'handlers': {
        'file': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'class': 'logging.FileHandler',
            'filename': 'reservation.log',
            'formatter': 'verbose'
        },
        'debug': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'class': 'logging.FileHandler',
            'filename': 'logs/reservation-debug.log',
            'formatter': 'verbose'
        },
        },
    'loggers': {
        'django': {
            'handlers':['file'],
            'propagate': True,
            'level':'DEBUG' if DEBUG else 'INFO',
            },
        'debug': {
            'handlers': ['debug'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            },
        }
}


################ CONFIG SERVER ###########

# Trust our nginx server
USE_X_FORWARDED_HOST = True

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken'
)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ALLOWED_HOSTS = os.environ.get('SITE_DOMAIN', '').split('|')
MY_SITE_DOMAIN = ALLOWED_HOSTS[0]
SITE_ID = 1
SITE_URL = '{0}://{1}'.format(os.environ.get('SITE_PROTOCOL', ''),
                              MY_SITE_DOMAIN)


###### CONFIG MEMCACHE #########

# Memcache
# try:
#     # pylint:disable=unused-import,import-error,wrong-import-position
#     import memcache
#     # pylint:enable=unused-import,import-error,wrong-import-position
#
#     CACHES = {
#         'default': {
#             'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#             'LOCATION': os.environ['MEMCACHE_HOSTS'].split('|'),
#             'KEY_PREFIX': os.environ['MEMCACHE_PREFIX'],
#         },
#     }
#
# except (ImportError, KeyError):
#     pass


############## CONFIG EMAIL BACKEND ###########

EMAIL_BACKEND="djmail.backends.async.EmailBackend"
DJMAIL_REAL_BACKEND="django.core.mail.backends.smtp.EmailBackend"

EMAIL_HOST = os.environ.get('EMAIL_HOST', 'smtp.gmail.com')
EMAIL_PORT = os.environ.get('EMAIL_PORT', 587)
DEFAULT_FROM_EMAIL = os.environ.get('EMAIL', 'testnubiquo@gmail.com')

EMAIL_HOST_USER = DEFAULT_FROM_EMAIL
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_PASS', 'nubiquo1234567890')
EMAIL_USE_TLS = True


##################### CONFIG CELERY TASK ###########

from datetime import timedelta

BROKER_URL = "amqp://{user}:{password}@rabbitmq:5672//?heartbeat=30".format(
    user=os.environ.get('RABBITMQ_DEFAULT_USER'),
    password=os.environ.get('RABBITMQ_DEFAULT_PASS')
)

BACKUP_FOLDER = os.path.join(BASE_DIR, 'backups')

CELERYBEAT_SCHEDULE = {
    'alert': {
        'task': 'reservation.tasks.alert',
        'schedule': timedelta(seconds=10),
        # 'schedule': crontab(minute=0, hour=0),
    },
    'occurrence_cancel': {
        'task': 'reservation.tasks.cancel_old_occurrences',
        'schedule': timedelta(seconds=10),
        # 'schedule': crontab(minute=0, hour=0),
    }
}

CELERY_TIMEZONE = 'UTC'


################ ADMINS SITE ####

# Recipients of traceback emails and other notifications.
ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)
MANAGERS = ADMINS


############### JET - ADMIN THEME CONFIG
JET_THEMES = [
    {
        'theme': 'default', # theme folder name
        'color': '#47bac1', # color of the theme's button in user menu
        'title': 'Default' # theme title
    },
    {
        'theme': 'green',
        'color': '#44b78b',
        'title': 'Green'
    },
    {
        'theme': 'light-green',
        'color': '#2faa60',
        'title': 'Light Green'
    },
    {
        'theme': 'light-violet',
        'color': '#a464c4',
        'title': 'Light Violet'
    },
    {
        'theme': 'light-blue',
        'color': '#5EADDE',
        'title': 'Light Blue'
    },
    {
        'theme': 'light-gray',
        'color': '#222',
        'title': 'Light Gray'
    }
]

JET_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboardDesktop'



############# SCHEDULER APP CONFIG
SHOW_CANCELLED_OCCURRENCES = False

OCCURRENCE_CANCEL_REDIRECT = '/scheduler/fullcalendar/treatments/'

TIME_INPUT_FORMATS = ('%I:%M %p',)
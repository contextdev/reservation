#!/usr/bin/env bash
sleep 20

echo "[run] make migrations"
python3 manage.py makemigrations || exit 1

echo "[run] Migrate DB"
python3 manage.py migrate || exit 1

echo "[run] Collect static files"
python3 manage.py collectstatic --noinput

echo "[run] Create superuser"
echo "from django.contrib.auth.models import User
from apps.schedule.models import Calendar
if not User.objects.filter(username='admin').count():
    user = User.objects.create_superuser('admin', 'contextinformatic@gmail.com', 'qwerty123')
    Calendar.objects.get_or_create(name='treatments')
" | python3 manage.py shell || exit 1

echo "[run] Load groups data"
python3 manage.py loaddata groups_data.json

#echo "[run] runserver"
/usr/local/bin/gunicorn reservation.wsgi:application -w 2 -b :8000 --reload

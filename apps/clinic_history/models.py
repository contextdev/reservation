from django.db import models
from apps.person.models import Patient
from apps.generic.models import GenericModel
from django.utils import timezone

class ClinicHistory(GenericModel):
    patient = models.ForeignKey(Patient)
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    attached = models.FileField(upload_to='history', blank=True)
    date = models.DateField()

    def __str__(self):
        return '%s , %s' %(self.title, self.patient.__str__())
from django import forms
from django.contrib import admin
from apps.person.models import Patient, Therapist
from django.core.exceptions import ObjectDoesNotExist


class ClinicHistoryAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ClinicHistoryAdminForm, self).__init__(*args, **kwargs)
        try:
            # We've added current user to the form from the admin
            therapist = Therapist.objects.get(user=self.current_user)
            self.fields['patient'].queryset = Patient.objects.filter(therapists__in=[therapist,])
        except ObjectDoesNotExist:
            pass

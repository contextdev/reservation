# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0001_initial'),
        ('clinic_history', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='clinichistory',
            name='patient',
            field=models.ForeignKey(to='person.Patient'),
        ),
    ]

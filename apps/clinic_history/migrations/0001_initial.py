# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('generic', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClinicHistory',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField(blank=True)),
                ('attached', models.FileField(upload_to='history', blank=True)),
                ('date', models.DateField()),
            ],
            bases=('generic.genericmodel',),
        ),
    ]

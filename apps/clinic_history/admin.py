from django.contrib import admin

from apps.generic.admin_widget import RelatedFieldCheckLimitByUser

from .models import ClinicHistory
from django.utils.html import format_html
from .forms import ClinicHistoryAdminForm


class ClinicHistoryAdmin(admin.ModelAdmin):
    # fields = ['title', 'patient', 'date', 'show_file']
    list_display = ['title', 'patient', 'date', 'show_file']
    ordering = ['patient', 'date']
    # TODO: Agregar date a los filtros
    list_filter = (('patient', RelatedFieldCheckLimitByUser),)
    readonly_fields = ('show_file', )
    search_fields = ('patient__first_name', 'patient__last_name')
    form = ClinicHistoryAdminForm

    def get_form(self, request, obj=None, **kwargs):
         form = super(ClinicHistoryAdmin, self).get_form(request, obj, **kwargs)
         form.current_user = request.user
         return form

    def show_file(self, instance):
        if instance.attached:
            return format_html('<a href="{url}" target="_blank"><button type="button" class="button button-secondary">Open File</button></>', url=instance.attached.url)
        else:
            return 'Has not file.'


    def get_queryset(self, request):
        try:
            user_therapist = request.user.therapist
            return ClinicHistory.objects.filter(patient__in=user_therapist.patients.all().values_list('id'))
        except:
            return ClinicHistory.objects.all()


admin.site.register(ClinicHistory, ClinicHistoryAdmin)
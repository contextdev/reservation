import json
import pytz
import datetime
import dateutil.parser
from django.utils.six.moves.urllib.parse import quote

from django.db.models import Q
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.http import HttpResponseRedirect, Http404
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
        UpdateView, CreateView, DeleteView, ModelFormMixin, ProcessFormView)
from django.utils.http import is_safe_url

from apps.schedule.conf.settings import (GET_EVENTS_FUNC, OCCURRENCE_CANCEL_REDIRECT,
                                    EVENT_NAME_PLACEHOLDER)
from apps.schedule.forms import EventForm, OccurrenceForm, NoteFormSet
from apps.schedule.models import Calendar, Occurrence, Event
from apps.schedule.periods import weekday_names
from apps.schedule.utils import check_event_permissions, check_calendar_permissions, coerce_date_dict

from apps.treatment.models import TreatmentType

from wkhtmltopdf.views import PDFTemplateView


class CalendarViewPermissionMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(CalendarViewPermissionMixin, cls).as_view(**initkwargs)
        return check_calendar_permissions(view)


class EventEditPermissionMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(EventEditPermissionMixin, cls).as_view(**initkwargs)
        return check_event_permissions(view)


class TemplateKwargMixin(TemplateResponseMixin):
    def get_template_names(self):
        if 'template_name' in self.kwargs:
            return [self.kwargs['template_name']]
        else:
            return super(TemplateKwargMixin, self).get_template_names()


class CancelButtonMixin(object):
    def post(self, request, *args, **kwargs):
        next_url = kwargs.get('next', None)
        self.success_url = get_next_url(request, next_url)
        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        else:
            return super(CancelButtonMixin, self).post(request, *args, **kwargs)


class CalendarMixin(CalendarViewPermissionMixin, TemplateKwargMixin):
    model = Calendar
    slug_url_kwarg = 'calendar_slug'


class CalendarView(CalendarMixin, DetailView):
    template_name = 'schedule/calendar.html'


from apps.person.models import Patient, Therapist, CalendarTherapist

class FullCalendarView(CalendarMixin, DetailView):
    template_name="schedule/fullcalendar.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(FullCalendarView, self).get_context_data()
        context['calendar_slug']=  kwargs.get('calendar_slug')

        try:
            user_therapist = self.request.user.therapist
            if user_therapist:
                context['patients'] = user_therapist.patients.all()
        except:
            context['patients'] = Patient.objects.all()
            context['therapists'] = Therapist.objects.all()

        if self.request.GET.get('therapist'):
            context['availability'] = CalendarTherapist.objects.filter(therapist=self.request.GET.get('therapist'))

        return context

from apps.payment.models import PaymentMethod

class OccurrencesListView(CalendarMixin, DetailView):
    template_name="schedule/occurrences_list.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        utc=pytz.UTC
        context = super(OccurrencesListView, self).get_context_data()

        if 'start' in self.request.GET:
            if '-' in self.request.GET.get('start'):
                convert = lambda d: datetime.datetime.strptime(d, '%Y-%m-%d')
            else:
                convert = lambda d: datetime.datetime.utcfromtimestamp(float(d))
            start = utc.localize(convert(self.request.GET.get('start')))
            end = utc.localize(convert(self.request.GET.get('end')))
        else:
            now = datetime.datetime.utcnow()
            start = datetime.datetime(now.year, now.month, now.day, 0, 0, 0, 0, pytz.UTC)
            end = start + datetime.timedelta(days=30)

        response_data =[]

        i = Occurrence.objects.latest('id').id + 1 if Occurrence.objects.all().count() > 0 else 1
        calendar = get_object_or_404(Calendar, slug=self.kwargs['calendar_slug'])
        event_list = calendar.events.filter(start__lte=end).filter(
            Q(end_recurring_period__gte=start) | Q(end_recurring_period__isnull=True) )


        therapist = self.request.GET['therapist'] if 'therapist' in self.request.GET else ''
        patient = self.request.GET['patient'] if 'patient' in self.request.GET else ''
        status = self.request.GET['status'] if 'status' in self.request.GET else None
        real_status = self.request.GET['real_status'] if 'real_status' in self.request.GET else None
        payment_method = self.request.GET['payment_method'] if 'payment_method' in self.request.GET else None

        total_amount = 0
        total_therapist_amount = 0
        # Check permission if user is therapist
        try:
            therapist_user = self.request.user.therapist
            therapist = str(therapist_user.id)
        except:
            therapist_user = None

        for event in event_list:
            occurrences = event.get_occurrences(start, end)

            for occurrence in occurrences:

                if occurrence.id:
                    occurrence_id = occurrence.id
                    existed = True
                else:
                    occurrence_id = i + occurrence.event.id
                    existed = False

                flag_occurrence = True # this flag define if this occurrence enter on the results

                # If exist a therapist and is diferent to occurrence therapist, exclude occurrence
                if therapist and str(occurrence.therapist.id) != therapist:
                    flag_occurrence = False

                # If exist a patient and is diferent to occurrence patient, exclude occurrence
                if patient and str(occurrence.patient.id) != patient:
                    flag_occurrence = False

                if real_status and str(occurrence.status) != real_status:
                    flag_occurrence = False

                # If exist param status, check
                if status:
                    if status == '1':
                        if occurrence.cancelled:
                            flag_occurrence = False
                    else:
                        if not occurrence.cancelled:
                            flag_occurrence = False

                # If exist param payment method
                if payment_method:
                    has_payment_method = False
                    for payment in occurrence.patient.payment_method.all():
                        if str(payment.id) == payment_method:
                            has_payment_method = True
                            break
                    flag_occurrence = has_payment_method

                if flag_occurrence:
                    total_therapist_amount += occurrence.get_therapist_fee()
                    total_amount += occurrence.get_price()

                    response_data.append({
                        "id": occurrence_id,
                        "cancelled": occurrence.cancelled,
                        "title": occurrence.title,
                        "start": occurrence.start,
                        "price": occurrence.get_price(),
                        "therapist_fee": occurrence.get_therapist_fee(),
                        "end": occurrence.end,
                        "description": occurrence.event.description,
                        "existed" : existed,
                        "event_id" : occurrence.event.id,
                        "color" : occurrence.event.color_event,
                        "url_cancel": occurrence.get_cancel_url(),
                        "url_edit": occurrence.get_edit_url(),
                        "therapist": occurrence.therapist.__str__(),
                        "therapist_id": occurrence.therapist.id,
                        "patient": occurrence.patient.__str__(),
                        "patient_id": occurrence.patient.id,
                        "status": occurrence.status
                    })

        context = {
            'calendar_slug': kwargs.get('calendar_slug'),
            'patients': therapist_user.patients.all() if therapist_user else Patient.objects.all(),
            'total_amount': total_amount,
            'total_therapist_amount': total_therapist_amount,
            'results': response_data,
            'start': start,
            'end': end,
            'STATUS_CHOICES': ((0, 'Cancelled'), (1, 'Actives')),
            'REAL_STATUS_CHOICES': ((0, 'Cancelled'), (1, 'Pending'), (2, 'Done')),
            'selected_patient': patient,
            'selected_therapist': therapist,
        }
        list_header = ['status', 'therapist', 'patient', 'start', 'end']
        if not therapist_user:
            list_header += ['price', 'therapist_fee']
            context['result_headers'] = list_header
            context['therapists'] = Therapist.objects.all()
            context['payment_methods'] = PaymentMethod.objects.all()
        else:
            context['result_headers'] = list_header

        return context


class OccurrenceListToPdf(OccurrencesListView, PDFTemplateView):
    filename = 'my_pdf.pdf'
    show_content_in_browser = True
    template_name = 'schedule/occurrence_list_to_pdf.html'


    def render_to_response(self, context, **response_kwargs):
        if self.request.user:
            context['auth_user'] = self.request.user
            return super(OccurrenceListToPdf, self).render_to_response(context, **response_kwargs)
        else:
            return super(OccurrenceListToPdf, self).render_to_response(context, **response_kwargs)


class CalendarByPeriodsView(CalendarMixin, DetailView):
    template_name = 'schedule/calendar_by_period.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(request, **kwargs)
        return self.render_to_response(context)

    def get_context_data(self, request, **kwargs):
        context = super(CalendarByPeriodsView, self).get_context_data(**kwargs)
        calendar = self.object
        periods = kwargs.get('periods', None)
        try:
            date = coerce_date_dict(request.GET)
        except ValueError:
            raise Http404
        if date:
            try:
                date = datetime.datetime(**date)
            except ValueError:
                raise Http404
        else:
            date = timezone.now()
        event_list = GET_EVENTS_FUNC(request, calendar)

        if 'django_timezone' in self.request.session:
            local_timezone = pytz.timezone(request.session['django_timezone'])
        else:
            local_timezone = timezone.get_default_timezone()
        period_objects = {}
        for period in periods:
            if period.__name__.lower() == 'year':
                period_objects[period.__name__.lower()] = period(event_list, date, None, local_timezone)
            else:
                period_objects[period.__name__.lower()] = period(event_list, date, None, None, local_timezone)

        context.update({
            'date': date,
            'periods': period_objects,
            'calendar': calendar,
            'weekday_names': weekday_names,
            'here': quote(request.get_full_path()),
        })
        return context


class OccurrenceMixin(CalendarViewPermissionMixin, TemplateKwargMixin):
    model = Occurrence
    pk_url_kwarg = 'occurrence_id'
    form_class = OccurrenceForm


class OccurrenceEditMixin(EventEditPermissionMixin, OccurrenceMixin, CancelButtonMixin):
    success_url = '/scheduler/fullcalendar/treatments/'

    def get_initial(self):
        initial_data = super(OccurrenceEditMixin, self).get_initial()
        _, self.object = get_occurrence(**self.kwargs)
        return initial_data

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if self.object:
            note_form = NoteFormSet(instance=self.object)
        else:
            note_form = NoteFormSet()
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=note_form))

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if self.object:
            note_form = NoteFormSet(self.request.POST, instance=self.object)
        if (form.is_valid() and note_form.is_valid()):
            return self.form_valid(form, note_form)
        else:
            return self.form_invalid(form, note_form)

    def form_valid(self, form, note_form):
        self.object = form.save(commit=True)
        note_form.instance = self.object
        note_form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, note_form):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  formset=note_form))


class OccurrenceView(OccurrenceMixin, DetailView):
    template_name = 'schedule/occurrence.html'


class OccurrencePreview(OccurrenceMixin, ModelFormMixin, ProcessFormView):
    template_name = 'schedule/occurrence.html'

    def get_context_data(self, **kwargs):
        context = super(OccurrencePreview, self).get_context_data()
        context = {
            'event': self.object.event,
            'occurrence': self.object,
        }
        return context


class EditOccurrenceView(OccurrenceEditMixin, UpdateView):
    success_url = '/scheduler/fullcalendar/treatments/'
    template_name = 'schedule/edit_occurrence.html'


class CreateOccurrenceView(OccurrenceEditMixin, CreateView):
    template_name = 'schedule/edit_occurrence.html'


from django.shortcuts import render

class CancelOccurrenceView(OccurrenceEditMixin, ModelFormMixin, ProcessFormView):
    template_name = 'schedule/cancel_occurrence.html'

    def post(self, request, *args, **kwargs):
        event, occurrence = get_occurrence(**kwargs)
        self.success_url = kwargs.get('next',
                        get_next_url(request, event.get_absolute_url()))
        if "cancel" not in request.POST:
            if occurrence.cancelled:
                occurrence.uncancel()
            else:
                if request.POST.get('reason_cancelation'):
                    occurrence.cancelation_reason = request.POST.get('reason_cancelation')
                    occurrence.cancel()
                else:
                    return render(request,
                                  'schedule/cancel_occurrence.html',
                                  {"occurrence": occurrence, "error": "The reason for cancelation is required"})
        return HttpResponseRedirect(self.success_url)


class EventMixin(CalendarViewPermissionMixin, TemplateKwargMixin):
    model = Event
    pk_url_kwarg = 'event_id'


class EventEditMixin(EventEditPermissionMixin, EventMixin, CancelButtonMixin):
    pass


class EventView(EventMixin, DetailView):
    template_name = 'schedule/event.html'


class EditEventView(EventEditMixin, UpdateView):
    form_class = EventForm
    template_name = 'schedule/create_event.html'


class CreateEventView(EventEditMixin, CreateView):
    form_class = EventForm
    template_name = 'schedule/create_event.html'

    def get_initial(self):
        date = coerce_date_dict(self.request.GET)
        initial_data = None
        if date:
            try:
                start = datetime.datetime(**date)
                initial_data = {
                    "start": start,
                    "end": start + datetime.timedelta(minutes=30)
                }
            except TypeError:
                raise Http404
            except ValueError:
                raise Http404
        return initial_data

    def form_valid(self, form):
        event = form.save(commit=False)
        event.creator = self.request.user
        event.calendar = get_object_or_404(Calendar, slug=self.kwargs['calendar_slug'])
        event.save()
        return HttpResponseRedirect(event.get_absolute_url())


class DeleteEventView(EventEditMixin, DeleteView):
    template_name = 'schedule/delete_event.html'

    def get_context_data(self, **kwargs):
        ctx = super(DeleteEventView, self).get_context_data(**kwargs)
        ctx['next'] = self.get_success_url()
        return ctx

    def get_success_url(self):
        """
        After the event is deleted there are three options for redirect, tried in
        this order:
        # Try to find a 'next' GET variable
        # If the key word argument redirect is set
        # Lastly redirect to the event detail of the recently create event
        """
        next_url = self.kwargs.get('next') or reverse('day_calendar', args=[self.object.calendar.slug])
        next_url = get_next_url(self.request, next_url)
        return next_url


def get_occurrence(event_id, occurrence_id=None, year=None, month=None, day=None, hour=None, minute=None, second=None):
    """
    Because occurrences don't have to be persisted, there must be two ways to
    retrieve them. both need an event, but if its persisted the occurrence can
    be retrieved with an id. If it is not persisted it takes a date to
    retrieve it.  This function returns an event and occurrence regardless of
    which method is used.
    """
    if(occurrence_id):
        occurrence = get_object_or_404(Occurrence, id=occurrence_id)
        event = occurrence.event
    elif(all((year, month, day, hour, minute, second))):
        event = get_object_or_404(Event, id=event_id)
        occurrence = event.get_occurrence(
            datetime.datetime(int(year), int(month), int(day), int(hour), int(minute), int(second)))
        if occurrence is None:
            raise Http404
    else:
        raise Http404
    return event, occurrence


def get_next_url(request, default):
    next_url = default
    if OCCURRENCE_CANCEL_REDIRECT:
        next_url = OCCURRENCE_CANCEL_REDIRECT
    _next_url = request.GET.get('next') if request.method in ['GET', 'HEAD'] else request.POST.get('next')
    if _next_url and is_safe_url(url=_next_url, host=request.get_host()):
        next_url = _next_url
    return next_url


def api_occurrences(request):
    utc=pytz.UTC
    # version 2 of full calendar
    if '-' in request.GET.get('start'):
        convert = lambda d: datetime.datetime.strptime(d, '%Y-%m-%d')
    else:
        convert = lambda d: datetime.datetime.utcfromtimestamp(float(d))
    start = utc.localize(convert(request.GET.get('start')))
    end = utc.localize(convert(request.GET.get('end')))
    #calendar = get_object_or_404(Calendar, slug=request.GET.get('calendar_slug'))
    calendar = Calendar.objects.first()
    response_data =[]
    if Occurrence.objects.all().count() > 0:
        i = Occurrence.objects.latest('id').id + 1
    else:
        i = 1
    event_list = calendar.events.filter(start__lte=end).filter(
        Q(end_recurring_period__gte=start) | Q(end_recurring_period__isnull=True) )

    therapist = request.GET['therapist'] if 'therapist' in request.GET else ''
    patient = request.GET['patient'] if 'patient' in request.GET else ''

    try:
        therapist_user = request.user.therapist
        therapist = str(therapist_user.id)
    except:
        therapist_user = None

    for event in event_list:
        occurrences = event.get_occurrences(start, end)

        for occurrence in occurrences:
            if occurrence.id:
                occurrence_id = occurrence.id
                existed = True
            else:
                occurrence_id = i + occurrence.event.id
                existed = False

            flag_occurrence = True
            if therapist and str(occurrence.therapist.id) != therapist:
                flag_occurrence = False

            if patient and str(occurrence.patient.id) != patient:
                flag_occurrence = False

            position = {}
            position['lat'] = str(occurrence.position.latitude) if occurrence.position else None
            position['lng'] = str(occurrence.position.longitude) if occurrence.position else None
            if flag_occurrence:
                response_data.append({
                    "id": occurrence_id,
                    "title":occurrence.patient.__str__(),
                    "start": occurrence.start.isoformat(),
                    "end": occurrence.end.isoformat(),
                    "description": occurrence.event.description,
                    "existed" : existed,
                    "event_id" : occurrence.event.id,
                    "color" : occurrence.event.color_event if not occurrence.cancelled else '#993920',
                    "url_cancel": occurrence.get_cancel_url(),
                    "url_edit": occurrence.get_edit_url(),
                    "therapist": occurrence.therapist.__str__(),
                    "therapist_id": occurrence.therapist.id,
                    "patient": occurrence.patient.__str__(),
                    "patient_id": occurrence.patient.id,
                    "cancelled": occurrence.cancelled,
                    "position": position#{'lat': str(occurrence.position.latitude), 'lng': str(occurrence.position.longitude)}
                    #"url_event_delete" : urlresolvers.reverse('admin:treatment_event_delete', args=(occurrence.event.id,)),
                    # "url_occurrence_cancel" : urlresolvers.reverse('cancel_occurrence_by_date', kwargs={
                    #     'event_id': occurrence.event.id,
                    #     'year': occurrence.start.year,
                    #     'month': occurrence.start.month,
                    #     'day' : occurrence.start.day,
                    #     'hour' : occurrence.start.hour,
                    #     'minute' : occurrence.start.minute,
                    #     'second' : occurrence.start.second }
                    # ),
                })

    return HttpResponse(json.dumps(response_data), content_type="application/json")

def api_move_or_resize_by_code(request):
    if request.method == 'POST':
        id = request.POST.get('id')
        existed = (request.POST.get('existed') == 'true')
        dt = datetime.timedelta(minutes=int(request.POST.get('delta')))
        resize = bool(request.POST.get('resize', False))
        resp = {}

        if existed:
            occurrence = Occurrence.objects.get(id=id)
            if not resize:
                occurrence.move(occurrence.start + dt, occurrence.end + dt)
            else:
                occurrence.move(occurrence.start, occurrence.end + dt)
        else:
            event_id = request.POST.get('event_id')
            event = Event.objects.get(id=event_id)
            if not resize:
                event.start += dt
            event.end = event.end + dt
            event.save()

        resp['status'] = "OK"

    return HttpResponse(json.dumps(resp))

def api_select_create(request):
    if request.method == 'POST':
        calendar_slug = request.POST.get('calendar_slug')
        start = dateutil.parser.parse(request.POST.get('start'))
        end = dateutil.parser.parse(request.POST.get('end'))

        calendar = Calendar.objects.get(slug=calendar_slug)
        event = Event.objects.create(
                                        start=start,
                                        end=end,
                                        title=EVENT_NAME_PLACEHOLDER,
                                        calendar=calendar,
                                        therapist=Therapist.objects.first(),
                                        patient=Patient.objects.first(),
                                        treatment_type= TreatmentType.objects.first(),
                                        suggested_price = 104
                                    )

        resp = {}
        resp['status'] = "OK"

    return HttpResponse(json.dumps(resp))

import datetime
from django.conf import settings
from django.contrib import admin
from apps.generic.admin_widget import RelatedFieldCheckLimitByUser, RelatedFieldCheckLimitisTherapist

from apps.schedule.models import Calendar, Event, CalendarRelation, Rule, Note
from apps.treatment.admin import TreatmentObjetiveInline
from apps.person.models import Therapist, Patient
from .forms import EventForm


class CalendarAdminOptions(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ['name']


class EventAdmin(admin.ModelAdmin):
    form = EventForm
    fields = ['therapist', 'patient', 'treatment_type', 'start', 'end', 'rule', 'end_recurring_period', 'description', 'alert', 'suggested_price']
    exclude = ['creator', 'calendar', 'color_event', 'title']
    list_display = ['patient', 'therapist', 'treatment_type', 'start', 'end', 'rule', 'end_recurring_period']
    inlines = [TreatmentObjetiveInline,]
    list_filter = (
        ('therapist', RelatedFieldCheckLimitisTherapist),
        ('patient', RelatedFieldCheckLimitByUser),
        ('treatment_type')
    )
    #readonly_fields = ('patient',)

    def get_readonly_fields(self, request, obj=None):
        if obj: # editing an existing object
            return self.readonly_fields + ('patient', 'start', 'end', 'rule', 'end_recurring_period')
        return self.readonly_fields


    def get_form(self, request, obj=None, **kwargs):
        # Add user to the form so we ca filter Patients by therapist
        form = super(EventAdmin, self).get_form(request, obj, **kwargs)
        form.current_user = request.user
        #if request.user.therapist:
        #    import pdb; pdb.set_trace()
        #    form['patient']['read_only'] = True
        return form

    def get_queryset(self, request):
        try:
            user_therapist = request.user.therapist
            return Event.objects.filter(therapist=user_therapist.id)
        except:
            return Event.objects.all()

    def get_changeform_initial_data(self, request):
        initial_data = {}

        start = request.GET.get('initial_start')
        if start:
            initial_data['start'] = datetime.datetime.strptime(start, "%Y-%m-%dT%H:%M")
            initial_data['end'] = initial_data['start'] + datetime.timedelta(hours=1)

        therapist = request.GET.get('therapist')
        if therapist:
            try:
                initial_data['therapist'] = Therapist.objects.get(pk=therapist)
            except Therapist.DoesNotExist:
                pass

        patient = request.GET.get('patient')
        if patient:
            try:
                initial_data['patient'] = Patient.objects.get(pk=patient)
            except Therapist.DoesNotExist:
                pass

        return initial_data if initial_data else super(EventAdmin, self).get_changeform_initial_data(request)




class NoteInline(admin.TabularInline):
    model = Note
    extra = 1
    exclude = ('occurrence', )
    fk_name = 'patient'


class NoteAdmin(admin.ModelAdmin):
    list_display = ['description_show', 'created', 'patient', 'turn']
    readonly_fields = ('patient', 'occurrence')
    list_filter = (('patient', RelatedFieldCheckLimitByUser),)

    def turn(self, obj):
        return obj.occurrence if obj.occurrence else None

    def description_show(self, obj):
        return obj.description[:60] + "..."

    def get_queryset(self, request):
        try:
            user_therapist = request.user.therapist
            return Note.objects.filter(patient__in=user_therapist.patients.all().values_list('id'))
        except:
            return Note.objects.all()


admin.site.register(Event, EventAdmin)
admin.site.register(Note, NoteAdmin)


if settings.SITE_ID == 1:
    admin.site.register([Rule, CalendarRelation])
    admin.site.register(Calendar, CalendarAdminOptions)
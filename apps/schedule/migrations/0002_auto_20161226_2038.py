# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
        ('treatment', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventrelation',
            name='event',
            field=models.ForeignKey(verbose_name='event', to='treatment.Event'),
        ),
        migrations.AddField(
            model_name='calendarrelation',
            name='calendar',
            field=models.ForeignKey(verbose_name='calendar', to='schedule.Calendar'),
        ),
        migrations.AddField(
            model_name='calendarrelation',
            name='content_type',
            field=models.ForeignKey(to='contenttypes.ContentType'),
        ),
    ]

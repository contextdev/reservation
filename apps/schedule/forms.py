from django import forms
from django.contrib.admin.widgets import AdminDateWidget, AdminTimeWidget
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from django.forms import inlineformset_factory, BaseInlineFormSet
from django.forms.widgets import HiddenInput
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from apps.schedule.models import Event, Occurrence, Note
from apps.person.models import Patient, Therapist


class AdminSplitDateTime(forms.SplitDateTimeWidget):
    """
    A SplitDateTime Widget that has some admin-specific styling.
    """
    def __init__(self, attrs=None):
        widgets = [AdminDateWidget, AdminTimeWidget] #AdminTimeWidget(attrs={'size': 6})
        # Note that we're calling MultiWidget, not SplitDateTimeWidget, because
        # we want to define widgets.
        forms.MultiWidget.__init__(self, widgets, attrs)


    def format_output(self, rendered_widgets):
        return format_html('<p class="datetime">{} {}<br />{} {}</p>',
                           _('Date:'), rendered_widgets[0],
                           _('Time:'), rendered_widgets[1])


class SpanForm(forms.ModelForm):
    start = forms.SplitDateTimeField(label=_("start"), widget=AdminSplitDateTime)
    end = forms.SplitDateTimeField(label=_("end"),
                                   help_text=_(u"The end time must be later than start time."),
                                   widget=AdminSplitDateTime)

    def clean(self):
        if 'end' in self.cleaned_data and 'start' in self.cleaned_data:
            if self.cleaned_data['end'] <= self.cleaned_data['start']:
                raise forms.ValidationError(_(u"The end time must be later than start time."))
        return self.cleaned_data



class SpanWidget(forms.Widget):
    def render(self, name, value, attrs=None):
        patient = Patient.objects.get(pk=value)
        return mark_safe('<span>%s</span>' % (patient.__str__()))

    def value_from_datadict(self, data, files, name):
        return self.original_value


class EventForm(SpanForm):
    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        try:
            # We've added current user to the form from the admin
            therapist = self.current_user.therapist
            self.fields['suggested_price'].widget=HiddenInput()
            self.fields['therapist'].initial = therapist.pk
            self.fields['therapist'].widget=HiddenInput()
            # self.fields['patient'].widget = SpanWidget(attrs={'readonly': 'readonly'}) #Patient.objects.filter(therapists__in=[therapist,])
        except Therapist.DoesNotExist:
            pass

    def clean(self):
        #run the standard clean method first
        cleaned_data=super(EventForm, self).clean()
        if self.instance:
            patient = getattr(self.instance, 'patient', None)
            if patient:
                therapist = self.cleaned_data['therapist']

                if therapist not in patient.therapists.all():
                    errors = {}
                    errors['therapist'] = forms.ValidationError("This patient is not assign to this therapist. You can add it in the patient detail section", code='code')
                    raise forms.ValidationError(errors)

        return cleaned_data

    # end_recurring_period = forms.DateTimeField(label=_(u"End recurring period"),
    #                                            help_text=_(u"This date is ignored for one time only events."),
    #                                            required=False)

    def clean_patient(self):
        patient = Patient.objects.get(pk=self.data['patient'])
        therapist = Therapist.objects.get(pk=self.data['therapist'])
        if therapist not in patient.therapists.all():
            raise forms.ValidationError("This patient is not assign to this therapist. You can add it in the patient detail section")
        return self.cleaned_data.get('patient', '')

    class Meta(object):
        model = Event
        exclude = ('creator', 'created_on', 'calendar')



class OccurrenceForm(SpanForm):
    class Meta(object):
        model = Occurrence
        fields = ('therapist', 'status', 'start', 'end', 'description', 'price', 'therapist_fee', 'position')
        exclude = ('original_start', 'original_end', 'event', 'cancelled', 'cancelation_reason', 'title', 'patient')


class OccurrenceAdminForm(forms.ModelForm):
    class Meta(object):
        model = Occurrence
        exclude = ('original_start', 'original_end', 'event', 'cancelled')


class NoteInline(BaseInlineFormSet):
    model = Note

    def __init__(self, *args, **kwargs):
        super(NoteInline, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        for form in self.forms:
            form.instance.patient = self.instance.patient if self.instance.patient else self.instance.event.patient

        return super(NoteInline, self).save()


NoteFormSet = inlineformset_factory(Occurrence, Note, fields=('description', 'occurrence'), extra=1, formset=NoteInline)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('generic', '__first__'),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('message', models.TextField()),
                ('user_id', models.PositiveIntegerField()),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(related_name='notifications', to='contenttypes.ContentType')),
                ('user_ct', models.ForeignKey(related_name='usernotifications', to='contenttypes.ContentType')),
            ],
            bases=('generic.genericmodel',),
        ),
    ]

from django.db import models
from apps.generic.models import GenericModel
from django.contrib.auth.models import User
from django.core import urlresolvers
from django.contrib.auth.models import Group
from phonenumber_field.modelfields import PhoneNumberField


class Person(GenericModel):
    class Meta:
        ordering = ['last_name', 'first_name']

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birth_date = models.DateField(verbose_name='D.O.B')
    email = models.EmailField()
    phone = PhoneNumberField(help_text="e.g '+41524204242'")
    address = models.CharField(max_length=255)

    class Meta:
        abstract = True

    def __str__(self):
        return "%s, %s" %(self.last_name.title(), self.first_name.title())


class City(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.name


OPTION_FREQUENCY = (
    ('daily', "Daily"),
    ('weekly', "Weekly"),
    ('monthly', "Monthly"),
)

class Supervisor(Person):
    pass



class Therapist(Person):
    user = models.OneToOneField(User, related_name='therapist')
    preferred_cities = models.ManyToManyField(City, related_name='therapists', help_text='Preferred Working cities')
    hourly_rate = models.FloatField()
    cpr_expiration = models.DateField(help_text='CPR expiration date')
    license_expiration = models.DateField(help_text='Profesional License expiration date')
    tb_test_expiration = models.DateField(help_text='TB test expiration date')
    supervisors = models.ManyToManyField(Supervisor, related_name='therapists')

    def get_absolute_url(self):
        return urlresolvers.reverse('admin:person_therapist_change', args=(self.id,))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)
        g = Group.objects.get(name='therapist')
        self.user.is_staff = True
        g.user_set.add(self.user)
        self.user.first_name = self.first_name
        self.user.last_name = self.last_name
        self.user.email = self.email
        self.user.save()


class Patient(Person):
    client_id = models.CharField(max_length=100, unique=True)
    frequency_service = models.CharField(max_length=100, choices=OPTION_FREQUENCY, help_text='Just for refence.')
    address1 = models.CharField(max_length=255)
    authorization = models.DateField(null=True, blank=True, help_text='End authorization date')
    therapists = models.ManyToManyField(Therapist,
                                        related_name='patients',
                                        help_text='List of Therapist that can see info about the client or '
                                                  'chedule appointments.')
    payment_method = models.ManyToManyField('payment.PaymentMethod', related_name='patients')

    def get_absolute_url(self):
        return urlresolvers.reverse('admin:person_patient_change', args=(self.id,))

    def get_full_name(self):
        return "%s, %s" %(self.last_name, self.first_name)

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'
        ordering = ['last_name', 'first_name']


class ContactPerson(Person):
    patient = models.ForeignKey(Patient, related_name='contact_persons')


class Caregiver(Person):
    patient = models.ForeignKey(Patient, related_name='caregivers')


DAY_CHOICES = (
    (0, 'Sunday'),
    (1, 'Mondy'),
    (2, 'Tuesday'),
    (3, 'Wednesday'),
    (4, 'Thursday'),
    (5, 'Friday'),
    (6, 'Saturday')
)

class CalendarTherapist(GenericModel):
    therapist = models.ForeignKey(Therapist, related_name='availability')
    day = models.IntegerField(default=1, choices=DAY_CHOICES)
    hour_init = models.TimeField()
    hour_end = models.TimeField()

    class Meta:
        ordering = ['day']

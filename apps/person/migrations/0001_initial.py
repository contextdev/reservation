# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import phonenumber_field.modelfields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('generic', '__first__'),
        ('payment', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CalendarTherapist',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('day', models.IntegerField(default=1, choices=[(0, 'Sunday'), (1, 'Mondy'), (2, 'Tuesday'), (3, 'Wednesday'), (4, 'Thursday'), (5, 'Friday'), (6, 'Saturday')])),
                ('hour_init', models.TimeField()),
                ('hour_end', models.TimeField()),
            ],
            options={
                'ordering': ['day'],
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='Caregiver',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('birth_date', models.DateField(verbose_name='D.O.B')),
                ('email', models.EmailField(max_length=254)),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(help_text="e.g '+41524204242'", max_length=128)),
                ('address', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='ContactPerson',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('birth_date', models.DateField(verbose_name='D.O.B')),
                ('email', models.EmailField(max_length=254)),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(help_text="e.g '+41524204242'", max_length=128)),
                ('address', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('birth_date', models.DateField(verbose_name='D.O.B')),
                ('email', models.EmailField(max_length=254)),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(help_text="e.g '+41524204242'", max_length=128)),
                ('address', models.CharField(max_length=255)),
                ('client_id', models.CharField(max_length=100, unique=True)),
                ('frequency_service', models.CharField(help_text='Just for refence.', max_length=100, choices=[('daily', 'Daily'), ('weekly', 'Weekly'), ('monthly', 'Monthly')])),
                ('address1', models.CharField(max_length=255)),
                ('authorization', models.DateField(help_text='End authorization date', null=True, blank=True)),
                ('payment_method', models.ManyToManyField(related_name='patients', to='payment.PaymentMethod')),
            ],
            options={
                'verbose_name_plural': 'Clients',
                'ordering': ['last_name', 'first_name'],
                'verbose_name': 'Client',
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='Supervisor',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('birth_date', models.DateField(verbose_name='D.O.B')),
                ('email', models.EmailField(max_length=254)),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(help_text="e.g '+41524204242'", max_length=128)),
                ('address', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='Therapist',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('birth_date', models.DateField(verbose_name='D.O.B')),
                ('email', models.EmailField(max_length=254)),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(help_text="e.g '+41524204242'", max_length=128)),
                ('address', models.CharField(max_length=255)),
                ('hourly_rate', models.FloatField()),
                ('cpr_expiration', models.DateField(help_text='CPR expiration date')),
                ('license_expiration', models.DateField(help_text='Profesional License expiration date')),
                ('tb_test_expiration', models.DateField(help_text='TB test expiration date')),
                ('preferred_cities', models.ManyToManyField(help_text='Preferred Working cities', related_name='therapists', to='person.City')),
                ('supervisors', models.ManyToManyField(related_name='therapists', to='person.Supervisor')),
                ('user', models.OneToOneField(related_name='therapist', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.AddField(
            model_name='patient',
            name='therapists',
            field=models.ManyToManyField(help_text='List of Therapist that can see info about the client or chedule appointments.', related_name='patients', to='person.Therapist'),
        ),
        migrations.AddField(
            model_name='contactperson',
            name='patient',
            field=models.ForeignKey(related_name='contact_persons', to='person.Patient'),
        ),
        migrations.AddField(
            model_name='caregiver',
            name='patient',
            field=models.ForeignKey(related_name='caregivers', to='person.Patient'),
        ),
        migrations.AddField(
            model_name='calendartherapist',
            name='therapist',
            field=models.ForeignKey(related_name='availability', to='person.Therapist'),
        ),
    ]

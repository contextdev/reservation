from django import template

register = template.Library()

@register.filter
def can_filter_by_therapist(user):
    if user and user.is_authenticated:
        if user.is_superuser:
            return True
        else:
            try:
                therapist = user.therapist
                if therapist:
                    return False
            except:
                return True
            # for group in user.groups.all():
            #     if group.name == 'secretary' or group.name == 'admin':
            #         return True

    return False
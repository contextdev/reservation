from django.contrib import admin

from django.conf import settings
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from .models import Therapist, Patient, City, ContactPerson, Caregiver, Supervisor, CalendarTherapist
from apps.clinic_history.models import ClinicHistory

from apps.schedule.admin import NoteInline

from django.utils.html import format_html
import datetime
from django.contrib.auth.admin import UserChangeForm, AdminPasswordChangeForm, UserCreationForm

USER_MODEL_ADMIN_EXCLUDE = ['is_staff', 'is_superuser', 'user_permissions']





class ContactPersonAdminInline(admin.StackedInline):
    model = ContactPerson
    extra = 1
    fk_name = 'patient'


class CaregiverAdminInline(admin.StackedInline):
    model = Caregiver
    extra = 1
    fk_name = 'patient'
    # max_num = 1

class ClinicHistoryAdminInline(admin.TabularInline):
    model = ClinicHistory
    fk_name = 'patient'


class PatientAdmin(admin.ModelAdmin):
    list_display = ['client_id', 'first_name', 'last_name', 'phone', 'status', 'authorization']
    search_fields = ['first_name', 'last_name', 'client_id']
    ordering = ['first_name', 'last_name']
    inlines = [ContactPersonAdminInline, CaregiverAdminInline, NoteInline, ClinicHistoryAdminInline]

    def get_readonly_fields(self, request, obj=None):
        try:
            if request.user.therapist:
                return self.readonly_fields + ('client_id',
                                        'frequency_service',
                                        'address1',
                                        'authorization',
                                        'therapists',
                                        'payment_method', 'first_name', 'last_name', 'birth_date', 'email', 'phone', 'address')
        except Therapist.DoesNotExist:
            pass
        return self.readonly_fields


    def status(self, instance):
        if instance.authorization:
            one_month_from_now = datetime.date.today() + datetime.timedelta(days=14)
            now = datetime.date.today()
            if now >= instance.authorization:
                return format_html('<div title="The date authorization expire" style="background-color: #c14747; height: 20px; width:20px; border-radius: 50%;"></div>')
            if one_month_from_now >= instance.authorization:
                return format_html('<div title="The date authorization expire" style="background-color: #FFFF00; height: 20px; width:20px; border-radius: 50%;"></div>')
            else:
                return format_html('<div style="background-color: #82b982; height: 20px; width:20px; border-radius: 50%;"></div>')
        return format_html('<div style="background-color: #82b982; height: 20px; width:20px; border-radius: 50%;"></div>')

    status.admin_order_field = 'authorization'
    status.allow_tags = True



class CalendarTherapistInLine(admin.TabularInline):
    model = CalendarTherapist
    extra = 5
    fk_name = 'therapist'


class TherapistAdmin(admin.ModelAdmin):
    fields = ['user', 'first_name', 'last_name', 'birth_date', 'email', 'phone', 'address', 'preferred_cities', \
              'hourly_rate', 'cpr_expiration', 'license_expiration', 'tb_test_expiration', 'supervisors']
    list_display = ['first_name', 'last_name', 'phone', 'status']
    search_fields = ['first_name', 'last_name']
    # inlines = [SupervisorAdminInline, CalendarTherapistInLine]
    inlines = [CalendarTherapistInLine, ]

    def status(self, instance):
        now = datetime.date.today()
        detail = ''
        if now > instance.cpr_expiration:
            detail += 'CPR'

        if now > instance.license_expiration:
            detail += ', LICENSE'

        if now > instance.tb_test_expiration:
            detail += ', TB TEST'

        if detail:
            return format_html('<div title="The date/s {detail} expiration" style="background-color: #c14747; height: 20px; width:20px; border-radius: 50%;"></div>', detail=detail)
        else:
            return format_html('<div style="background-color: #82b982; height: 20px; width:20px; border-radius: 50%;"></div>')
        # return format_html_join(
        #     mark_safe('<br/>'),
        #     '{}',
        #     ((line,) for line in ['Mi direccion 1', 'Mi direccion2']),
        # ) or "<span class='errors'>I can't determine this address.</span>"


class ContactPersonAdmin(admin.ModelAdmin):
    list_filter = ('patient', )
    search_fields = ['first_name', 'last_name']


class CaregiverAdmin(admin.ModelAdmin):
    list_filter = ('patient', )
    search_fields = ['first_name', 'last_name']


class SupervisorAdmin(admin.ModelAdmin):
    search_fields = ['first_name', 'last_name']


class UserAdmin(UserAdmin):

    add_form_template = 'admin/auth/user/add_form.html'
    change_user_password_template = None
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Permissions', {'fields': ('is_active', 'groups')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
    )
    form = UserChangeForm
    add_form = UserCreationForm
    change_password_form = AdminPasswordChangeForm
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff')
    list_filter = ('is_active', 'groups')
    search_fields = ('username', 'first_name', 'last_name', 'email')
    ordering = ('username',)
    filter_horizontal = ('groups', 'user_permissions',)


if settings.SITE_ID != 1:
    admin.site.unregister(User)
    admin.site.register(User, UserAdmin)


admin.site.register(ContactPerson, ContactPersonAdmin)
admin.site.register(Caregiver, CaregiverAdmin)
admin.site.register(Supervisor, SupervisorAdmin)
admin.site.register(Patient, PatientAdmin)
admin.site.register(Therapist, TherapistAdmin)
admin.site.register(City)

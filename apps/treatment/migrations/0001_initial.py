# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import geoposition.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('generic', '__first__'),
        ('person', '0001_initial'),
        ('schedule', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('name', models.CharField(max_length=100)),
                ('slug', models.SlugField()),
            ],
            options={
                'verbose_name_plural': 'Objectives Categories',
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(null=True, verbose_name='title', blank=True, max_length=255)),
                ('start', models.DateTimeField(verbose_name='start')),
                ('end', models.DateTimeField(help_text='The end time must be later than the start time.', verbose_name='end')),
                ('end_recurring_period', models.DateTimeField(help_text='This date is ignored for one time only events.', verbose_name='end recurring period', blank=True, null=True)),
                ('description', models.TextField(verbose_name='description', blank=True, null=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='created on')),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name='updated on')),
                ('color_event', models.CharField(null=True, verbose_name='Color event', blank=True, max_length=10)),
                ('position', geoposition.fields.GeopositionField(max_length=42)),
                ('alert', models.PositiveIntegerField(default=0, choices=[(0, 'No alert'), (15, '15 min antes'), (30, '30 min antes'), (45, '45 min antes'), (60, '60 min antes')])),
                ('suggested_price', models.FloatField(help_text='Suggested prices to per session/turn')),
                ('calendar', models.ForeignKey(blank=True, verbose_name='calendar', to='schedule.Calendar', null=True)),
                ('creator', models.ForeignKey(related_name='creator', blank=True, verbose_name='creator', to=settings.AUTH_USER_MODEL, null=True)),
                ('patient', models.ForeignKey(related_name='events', to='person.Patient')),
                ('rule', models.ForeignKey(blank=True, verbose_name='recurrency', help_text="Select '----' for a one time only event.", to='schedule.Rule', null=True)),
                ('therapist', models.ForeignKey(related_name='events', to='person.Therapist')),
            ],
            options={
                'verbose_name_plural': 'Events',
                'verbose_name': 'Event',
            },
        ),
        migrations.CreateModel(
            name='GenericObjective',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('category', models.ForeignKey(related_name='objetives', to='treatment.Category')),
            ],
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('description', models.TextField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'notes',
                'verbose_name': 'note',
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='Occurrence',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(null=True, verbose_name='title', blank=True, max_length=255)),
                ('description', models.TextField(verbose_name='description', blank=True, null=True)),
                ('start', models.DateTimeField(verbose_name='start')),
                ('end', models.DateTimeField(verbose_name='end')),
                ('status', models.IntegerField(default=1, choices=[(0, 'Cancelled'), (1, 'Pending'), (2, 'Done')])),
                ('cancelled', models.BooleanField(default=False, verbose_name='cancelled')),
                ('original_start', models.DateTimeField(verbose_name='original start')),
                ('original_end', models.DateTimeField(verbose_name='original end')),
                ('created_on', models.DateTimeField(auto_now_add=True, verbose_name='created on')),
                ('updated_on', models.DateTimeField(auto_now=True, verbose_name='updated on')),
                ('price', models.FloatField(null=True)),
                ('therapist_fee', models.FloatField()),
                ('position', geoposition.fields.GeopositionField(max_length=42, blank=True, null=True)),
                ('cancelation_reason', models.CharField(max_length=200, blank=True, null=True)),
                ('event', models.ForeignKey(verbose_name='event', to='treatment.Event')),
                ('patient', models.ForeignKey(null=True, blank=True, to='person.Patient')),
                ('therapist', models.ForeignKey(null=True, blank=True, to='person.Therapist')),
            ],
            options={
                'verbose_name_plural': 'appointments',
                'verbose_name': 'appointment',
            },
        ),
        migrations.CreateModel(
            name='TreatmentObjetive',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('title', models.CharField(max_length=100, blank=True)),
                ('description', models.TextField(blank=True)),
                ('generic', models.ForeignKey(null=True, blank=True, to='treatment.GenericObjective')),
                ('treatment', models.ForeignKey(to='treatment.Event')),
            ],
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='TreatmentType',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('name', models.CharField(max_length=100)),
            ],
            bases=('generic.genericmodel',),
        ),
        migrations.AddField(
            model_name='note',
            name='occurrence',
            field=models.ForeignKey(related_name='notes', null=True, blank=True, to='treatment.Occurrence'),
        ),
        migrations.AddField(
            model_name='note',
            name='patient',
            field=models.ForeignKey(related_name='notes', to='person.Patient'),
        ),
        migrations.AddField(
            model_name='event',
            name='treatment_type',
            field=models.ForeignKey(verbose_name='type', to='treatment.TreatmentType'),
        ),
    ]

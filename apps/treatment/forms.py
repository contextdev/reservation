from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import TreatmentObjetive, GenericObjective



class TreatmentObjetiveForm(forms.ModelForm):
    generic = forms.ModelChoiceField(queryset=GenericObjective.objects.all(),
                                     required=False,
                                     label=_('Generic Objetive'),
                                     help_text=_("Select generic objetive or complete the next fields"))
    title = forms.CharField(max_length=50, required=False, label=_("Title"),
                            help_text=_("If not select generic objetive, this field is required"))
    description = forms.CharField(max_length=400, required=False, label=_("Description"), widget=forms.Textarea())

    def clean(self):
        super(TreatmentObjetiveForm, self).clean()
        generic = self.cleaned_data.get('generic')
        title = self.cleaned_data.get('title')
        description = self.cleaned_data.get('description')
        error = False

        if not generic:
            if not title:
                self._errors['title'] = self.error_class([_("If not select generic objetive, this field is required")])
                error = True

            if not description:
                error = True
                self._errors['description'] = self.error_class([_("If not select generic objetive, this field is required")])

            if not title and not description:
                error = True
                self._errors['description'] = self.error_class([_("If not complete title and description, this field is required")])

        if error:
            raise forms.ValidationError("Error")

        return self.cleaned_data

    class Meta:
        model = TreatmentObjetive
        fields = ['generic', 'title', 'description']

from django.db import models
from apps.generic.models import GenericModel

from apps.schedule.models import Event
#from mptt.models import MPTTModel, TreeForeignKey



class TreatmentType(GenericModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Category(GenericModel):
    name = models.CharField(max_length=100)
    slug = models.SlugField()

    class Meta:
        verbose_name_plural = 'Objectives Categories'

    def __str__(self):
        return self.name


class GenericObjective(GenericModel):
    title = models.CharField(max_length=100)
    description = models.TextField()
    category = models.ForeignKey(Category, related_name='objetives')

    @staticmethod
    def autocomplete_search_fields():
        return 'name', 'category__name'

    def __str__(self):
        return self.category.name + " " + self.title


class TreatmentObjetive(GenericModel):
    treatment = models.ForeignKey(Event)
    generic = models.ForeignKey(GenericObjective, null=True, blank=True)
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    #parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)



    def __str__(self):
        return self.title


from django.contrib import admin

from .forms import TreatmentObjetiveForm
from .models import TreatmentType, TreatmentObjetive, GenericObjective, Category



class TreatmentObjetiveInline(admin.TabularInline):
    model = TreatmentObjetive
    extra = 0
    form = TreatmentObjetiveForm

class GenericObjectiveAdmin(admin.ModelAdmin):
    list_display = ['title', 'category']


admin.site.register(GenericObjective, GenericObjectiveAdmin)
admin.site.register([TreatmentType, Category])

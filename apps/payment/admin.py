from django.contrib import admin

from .models import ClientBill, TherapistBill, PaymentMethod, TherapistPayment, \
    ClientPayment, ClientItemBill, TherapistItemBill, BILL_STATUS
from django.db.models import Sum

from django.utils.html import format_html


class GenericItemBillInLine(admin.TabularInline):
    #exclude = ('total_price' )
    readonly_fields = ('total_price',)
    extra = 1
    min_num = 1
    fk_name = 'bill'


class ClientItemBillInLine(GenericItemBillInLine):
    model = ClientItemBill


class TherapistItemBillInLine(GenericItemBillInLine):
    model = TherapistItemBill


class GenericPaymentInLine(admin.TabularInline):
    extra = 1
    fk_name = 'bill'


class ClientPaymentInLine(GenericPaymentInLine):
    model = ClientPayment


class TherapistPaymentInLine(GenericPaymentInLine):
    model = TherapistPayment


def make_payed(modeladmin, request, queryset):
        queryset.update(status='1')

make_payed.short_description = 'Mark selected billies as payed.'


class GenericBillAdmin(admin.ModelAdmin):
    list_display = ( 'payment_method', 'total', 'remaining')
    exclude = ['pay_date',]
    readonly_fields = ('total', 'remaining', 'created')
    actions = [make_payed]

    def remaining(self, instance):
        try:
            payment_amount = instance.payments.all().aggregate(Sum('price'))['price__sum']
            total_amount = instance.items.all().aggregate(Sum('total_price'))['total_price__sum']
            return (total_amount - payment_amount) if payment_amount else total_amount
        except:
            return '0.00'

    def total(self, instance):
        try:
            total_amount = instance.items.all().aggregate(Sum('total_price'))['total_price__sum']
            return total_amount
        except:
            return '0.00'

    # def status_bill(self, instance):
    #     if instance.status == 0:
    #         return format_html('<label style="color: #6f7e95; background-color: #d0dbe6; border-radius: 2px; padding:5px;" title="Created on {date}">{status}</label>', status=dict(BILL_STATUS)[instance.status])
    #     else:
    #         return format_html('<label style="color: #fff; background-color: #639af5;border-radius: 2px; padding: 5px;" title="Payed on {date}">{status}</label>', status=dict(BILL_STATUS)[instance.status])


class ClientBillAdmin(GenericBillAdmin):
    list_display = ('patient', 'date', 'payment_method', 'total', 'remaining')
    list_filter = ('patient', 'payment_method')
    inlines = [ClientItemBillInLine, ClientPaymentInLine ]



class TherapistBillAdmin(GenericBillAdmin):
    list_display = ('therapist', 'date', 'payment_method', 'total', 'remaining')
    list_filter = ('therapist', 'payment_method')
    inlines = [TherapistItemBillInLine, TherapistPaymentInLine ]



admin.site.register(PaymentMethod)
admin.site.register(ClientBill, ClientBillAdmin)
admin.site.register(TherapistBill, TherapistBillAdmin)



from django.db import models
from django.utils import timezone
from apps.person.models import Therapist, Patient
from apps.generic.models import GenericModel

import datetime


PAYMENT_METHOD_CHOICES = (
    ('efectivo', 'Efectivo'),
    ('obra social', 'Obra social'),
)

class PaymentMethod(models.Model):
    method = models.CharField(max_length=100, choices=PAYMENT_METHOD_CHOICES)
    name = models.CharField(max_length=100, help_text='eg. Sancor salud')

    def __str__(self):
        return "%s - %s" %(self.name, self.method)

BILL_STATUS = (
    (0, 'Emitido'),
    (1, 'Pagado')
)


class GenericBill(GenericModel):
    #status = models.IntegerField(choices=BILL_STATUS, default=0)
    date = models.DateField(default=timezone.now, blank=True)
    detail = models.TextField()
    payment_method = models.ForeignKey(PaymentMethod)

    class Meta:
        abstract = True

    # def __init__(self, *args, **kwargs):
    #     super(GenericBill, self).__init__(*args, **kwargs)
    #     self.__initial_status = self.status
    #
    # def save(self, *args, **kwargs):
    #     if self.pk:
    #         #origin = self.objects.get(pk=self.pk)
    #         if self.__initial_status != self.status and self.status == 1:
    #             self.pay_date = datetime.date.today()
    #     else:
    #         if self.status == 1:
    #             self.pay_date = datetime.date.today()
    #     super(GenericBill, self).save(*args, **kwargs)
    #     self.__initial_status = self.status


class TherapistBill(GenericBill):
    therapist = models.ForeignKey(Therapist, related_name='billies')


class ClientBill(GenericBill):
    patient = models.ForeignKey(Patient, related_name='billies')


class GenericItemBill(GenericModel):
    detail = models.CharField(max_length=100, blank=True)
    quantity = models.PositiveIntegerField(default=1)
    unit_price = models.FloatField()
    total_price = models.FloatField()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.total_price = self.quantity * self.unit_price
        super(GenericItemBill, self).save(*args, **kwargs)


class ClientItemBill(GenericItemBill):
    bill = models.ForeignKey(ClientBill, related_name='items')


class TherapistItemBill(GenericItemBill):
    bill = models.ForeignKey(TherapistBill, related_name='items')


class GenericPayment(GenericModel):
    date = models.DateField(blank=True)
    detail = models.CharField(max_length=255, blank=True)
    price = models.FloatField()
    
    class Meta:
       abstract = True
       

class ClientPayment(GenericPayment):
    bill = models.ForeignKey(ClientBill, related_name='payments')


class TherapistPayment(GenericPayment):
    bill = models.ForeignKey(TherapistBill, related_name='payments')




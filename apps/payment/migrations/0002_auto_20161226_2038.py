# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0001_initial'),
        ('payment', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='therapistbill',
            name='therapist',
            field=models.ForeignKey(related_name='billies', to='person.Therapist'),
        ),
        migrations.AddField(
            model_name='clientpayment',
            name='bill',
            field=models.ForeignKey(related_name='payments', to='payment.ClientBill'),
        ),
        migrations.AddField(
            model_name='clientitembill',
            name='bill',
            field=models.ForeignKey(related_name='items', to='payment.ClientBill'),
        ),
        migrations.AddField(
            model_name='clientbill',
            name='patient',
            field=models.ForeignKey(related_name='billies', to='person.Patient'),
        ),
        migrations.AddField(
            model_name='clientbill',
            name='payment_method',
            field=models.ForeignKey(to='payment.PaymentMethod'),
        ),
    ]

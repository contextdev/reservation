# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('generic', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientBill',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('date', models.DateField(default=django.utils.timezone.now, blank=True)),
                ('detail', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='ClientItemBill',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('detail', models.CharField(max_length=100, blank=True)),
                ('quantity', models.PositiveIntegerField(default=1)),
                ('unit_price', models.FloatField()),
                ('total_price', models.FloatField()),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='ClientPayment',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('date', models.DateField(blank=True)),
                ('detail', models.CharField(max_length=255, blank=True)),
                ('price', models.FloatField()),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='PaymentMethod',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('method', models.CharField(max_length=100, choices=[('efectivo', 'Efectivo'), ('obra social', 'Obra social')])),
                ('name', models.CharField(help_text='eg. Sancor salud', max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='TherapistBill',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('date', models.DateField(default=django.utils.timezone.now, blank=True)),
                ('detail', models.TextField()),
                ('payment_method', models.ForeignKey(to='payment.PaymentMethod')),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='TherapistItemBill',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('detail', models.CharField(max_length=100, blank=True)),
                ('quantity', models.PositiveIntegerField(default=1)),
                ('unit_price', models.FloatField()),
                ('total_price', models.FloatField()),
                ('bill', models.ForeignKey(related_name='items', to='payment.TherapistBill')),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
        migrations.CreateModel(
            name='TherapistPayment',
            fields=[
                ('genericmodel_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='generic.GenericModel')),
                ('date', models.DateField(blank=True)),
                ('detail', models.CharField(max_length=255, blank=True)),
                ('price', models.FloatField()),
                ('bill', models.ForeignKey(related_name='payments', to='payment.TherapistBill')),
            ],
            options={
                'abstract': False,
            },
            bases=('generic.genericmodel',),
        ),
    ]

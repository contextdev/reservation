from django.contrib import admin

class RelatedFieldCheckLimitByUser(admin.RelatedFieldListFilter):
    def field_choices(self, field, request, model_admin):
        try:
            user_therapist = request.user.therapist
            limit_choices_to = {'pk__in': set(user_therapist.patients.all().values_list('id', flat=True))}
        except:
            limit_choices_to = {}

        return field.get_choices(include_blank=False, limit_choices_to=limit_choices_to)


class RelatedFieldCheckLimitisTherapist(admin.RelatedFieldListFilter):
    def field_choices(self, field, request, model_admin):
        try:
            user_therapist = request.user.therapist
            limit_choices_to = {'pk__in': {user_therapist.id}}
        except:
            limit_choices_to = {}

        return field.get_choices(include_blank=False, limit_choices_to=limit_choices_to)
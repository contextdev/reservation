from django.contrib.admin import AdminSite, site

from django.contrib import admin
from .models import PinnedApplication, Bookmark
from django.conf import settings

# class UserDashboardModuleAdmin(admin.ModelAdmin):
#     list_display = ('title', 'user', 'site', )

if settings.SITE_ID == 1:
    #admin.site.register(UserDashboardModule, UserDashboardModuleAdmin)
    admin.site.register(PinnedApplication, admin.ModelAdmin)
    admin.site.register(Bookmark, admin.ModelAdmin)

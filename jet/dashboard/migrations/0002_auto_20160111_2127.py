# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.sites.managers


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('dashboard', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='userdashboardmodule',
            managers=[
                ('objects', django.contrib.sites.managers.CurrentSiteManager()),
            ],
        ),
        migrations.AddField(
            model_name='userdashboardmodule',
            name='site',
            field=models.ForeignKey(null=True, to='sites.Site'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.sites.managers


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('jet', '0002_delete_userdashboardmodule'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='bookmark',
            managers=[
                ('objects', django.contrib.sites.managers.CurrentSiteManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='pinnedapplication',
            managers=[
                ('objects', django.contrib.sites.managers.CurrentSiteManager()),
            ],
        ),
        migrations.AddField(
            model_name='bookmark',
            name='site',
            field=models.ForeignKey(null=True, to='sites.Site'),
        ),
        migrations.AddField(
            model_name='pinnedapplication',
            name='site',
            field=models.ForeignKey(null=True, to='sites.Site'),
        ),
    ]

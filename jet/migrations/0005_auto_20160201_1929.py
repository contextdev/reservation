# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jet', '0004_userdashboardmodule'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userdashboardmodule',
            name='site',
        ),
        migrations.DeleteModel(
            name='UserDashboardModule',
        ),
    ]

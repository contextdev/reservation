# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.sites.managers


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('jet', '0003_auto_20160111_2127'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserDashboardModule',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='Title', max_length=255)),
                ('module', models.CharField(verbose_name='module', max_length=255)),
                ('app_label', models.CharField(null=True, verbose_name='application name', max_length=255, blank=True)),
                ('user', models.PositiveIntegerField(verbose_name='user')),
                ('column', models.PositiveIntegerField(verbose_name='column')),
                ('order', models.IntegerField(verbose_name='order')),
                ('settings', models.TextField(verbose_name='settings', blank=True, default='')),
                ('children', models.TextField(verbose_name='children', blank=True, default='')),
                ('collapsed', models.BooleanField(verbose_name='collapsed', default=False)),
                ('site', models.ForeignKey(null=True, to='sites.Site')),
            ],
            options={
                'verbose_name_plural': 'user dashboard modules',
                'verbose_name': 'user dashboard module',
                'ordering': ('column', 'order'),
            },
            managers=[
                ('objects', django.contrib.sites.managers.CurrentSiteManager()),
            ],
        ),
    ]

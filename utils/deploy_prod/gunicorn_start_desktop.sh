#!/bin/bash
NAME="reservation"                                                  # Name of the application
ENVNAME=environment                                                 # Name of virtualenv
DJANGODIR=/webapps/reservation/reservation
SOCKFILE=/webapps/reservation/gunicorn_desktop.sock                 # we will communicte using this unix socket
USER=reservation                                                    # the user to run as
GROUP=webapps                                                       # the group to run as
NUM_WORKERS=3                                                       # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=reservation.settings.production_desktop      # which settings file should Django use
DJANGO_WSGI_MODULE=reservation.wsgi                                 # WSGI module name


cd $DJANGODIR
source ../$ENVNAME/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec ../$ENVNAME/bin/gunicorn --preload ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --bind=unix:$SOCKFILE \
  --log-level=debug \
  --log-file=-

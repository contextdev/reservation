#!/bin/bash
NAME="reservation_cron"
ENVNAME=environment                                                 # Name of virtualenv
DJANGODIR=/webapps/reservation/reservation
USER=reservation                                                    # the user to run as
GROUP=webapps                                                       # the user to run as
DJANGO_SETTINGS_MODULE=reservation.settings.production              # which settings file should Django use

cd $DJANGODIR
source ../$ENVNAME/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
      
exec celery -A reservation beat


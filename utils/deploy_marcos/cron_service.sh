#!/bin/bash
ENVNAME=env                                                   # Name of virtualenv
DJANGODIR=/webapps/reservation/reservation
USER=m                                                     # the user to run as
DJANGO_SETTINGS_MODULE=reservation.settings.local                    # which settings file should Django use

cd $DJANGODIR
source ../$ENVNAME/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
      
exec celery -A reservation beat

